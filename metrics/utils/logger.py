"""
Nameko Logstash Dependency Provider
"""
import logging
from logstash_async.handler import AsynchronousLogstashHandler

from nameko.extensions import DependencyProvider


class LogstashDependency(DependencyProvider):
    def setup(self):
        config = self.container.config.get('LOGSTASH', {})

        self.host = config.get('HOST')
        self.port = config.get('PORT')
        self.log_level = getattr(
            logging, config.get('LOG_LEVEL')
        )
        self.logger_by_service_name = dict()

    def get_dependency(self, *args, **kwargs):
        worker_ctx = args[0]
        service_name = worker_ctx.service_name
        if not self.logger_by_service_name.get(service_name):
            logger = logging.getLogger(service_name)
            self.logger_by_service_name[service_name] = logger
            logger.setLevel(self.log_level)
            handler = AsynchronousLogstashHandler(
                self.host,
                self.port,
                database_path='logstash.db'
            )
            logger.addHandler(handler)

        return self.logger_by_service_name[service_name]
