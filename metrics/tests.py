import unittest
import pytz
from datetime import datetime, timedelta
from freezegun import freeze_time
from nameko.testing.services import worker_factory
from .service import InfluxdbMetricsService


class BaseInfluxDBTestCase:
    @property
    def points(self):
        return [
            {
                'fields': {
                    'value': 1.2,
                },
                'tags': {
                    'tag1': 'value1',
                    'tag2': 'value2'
                }
            },
            {
                'fields': {
                    'value': 1.86,
                },
                'tags': {
                    'tag1': 'value2',
                    'tag2': 'value1'
                }
            },
            {
                'fields': {
                    'value': 1.4,
                },
                'tags': {
                    'tag1': 'value3',
                    'tag2': 'value2'
                }
            },
            {
                'fields': {
                    'value': 1.8,
                },
                'tags': {
                    'tag1': 'value1',
                    'tag2': 'value2'
                }
            },
        ]


class InfluxDBMetricsServiceTestCase(BaseInfluxDBTestCase, unittest.TestCase):
    def setUp(self):
        self.service = worker_factory(InfluxdbMetricsService)
        self.tearDown()

    def test_write(self):
        points = self.points
        self.service.write_points(
            measurement='testdb',
            points=points,
        )
        results = self.service.read(measurement='testdb')
        self.assertEqual(len(results), len(points))
        for i in range(len(results)):
            self.assertEqual(
                results[i]['value'],
                points[i]['fields']['value']
            )
        self.tearDown()

    def test_select(self):
        points = self.points
        self.service.write_points(
            measurement='testdb',
            points=points
        )
        results = self.service.read(measurement='testdb')
        self.assertEqual(len(results), len(points))
        for i in range(len(results)):
            self.assertEqual(
                results[i]['value'],
                points[i]['fields']['value']
            )
        self.tearDown()

    def test_time_range_filter(self):
        points = self.points
        dts = (
            datetime(
                2018, 10, day + 1, 10
            ).replace(
                tzinfo=pytz.utc
            )
            for day in range(len(points))
        )
        utc = []
        for i in range(len(points)):
            with freeze_time(dts):
                u = datetime.utcnow()
                u = u.replace(tzinfo=pytz.utc)
                utc.append(u)
                self.service.write(
                    measurement='testdb',
                    fields=points[i]['fields'],
                    tags=points[i]['tags']
                )
        for i in range(len(utc)):
            results = self.service.read(
                measurement='testdb',
                time_range=[
                    utc[i] - timedelta(hours=12)
                ]
            )
            self.assertEqual(len(results), (len(utc) - i))
        self.tearDown()

    def tearDown(self):
        self.service.drop('testdb')
