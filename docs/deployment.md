# Deployment

Code is setup to use environment variables for all settings. Please set all of the following as needed.

```
RABBITMQ_DEFAULT_USER: admin
RABBITMQ_DEFAULT_PASS: adminpass
RABBITMQ_HOST: broker
LOGSTASH_HOST: logstash
LOGSTASH_PORT: 5000
INFLUXDB_HOST: metrics
INFLUXDB_DB: metrics
INFLUXDB_USER: admin
INFLUXDB_USER_PASSWORD: adminpass
```

If you want to run it as a container, a docker-compose service will look something like this:

```
service:
    image: registry.gitlab.com/drkloc/nameko.influxdb.metrics:latest
    environment:
      RABBITMQ_DEFAULT_USER: admin
      RABBITMQ_DEFAULT_PASS: adminpass
      RABBITMQ_HOST: broker
      LOGSTASH_HOST: logstash
      LOGSTASH_PORT: 5000
      INFLUXDB_HOST: metrics
      INFLUXDB_DB: metrics
      INFLUXDB_USER: admin
      INFLUXDB_USER_PASSWORD: adminpass
    restart: always
```